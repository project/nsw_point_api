<?php

declare(strict_types = 1);

namespace Drupal\nsw_point_api\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\nsw_point_api\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [$this->getConfigId()];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'nsw_point_api_settings';
  }

  /**
   * A method to get the configuration Id for the form.
   *
   * @return string
   */
  private function getConfigId(): string {
    return 'nsw_point_api.settings';
  }

  /**
   * A method to get the configuration for the form.
   *
   * @return \Drupal\Core\Config\Config
   */
  private function getConfig(): Config {
    return $this->config($this->getConfigId());
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API key'),
      '#key_filters' => [
        'type_group' => 'authentication',
      ],
      '#default_value' => $this->getConfig()->get('api_key'),
    ];
    $form += parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->cleanValues();
    $this->getConfig()->set('api_key', $form_state->getValue('api_key'))->save();
  }

}
