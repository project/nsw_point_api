<?php

declare(strict_types = 1);

namespace Drupal\nsw_point_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\key\KeyRepositoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ApiClient.
 *
 * @package Drupal\nsw_point_api
 */
final class ApiClient {

  /**
   * @var string
   */
  const BASE_URI = 'https://point.digital.nsw.gov.au';

  /**
   * @var string
   */
  const GET_ADDRESSES_ENDPOINT = '/v2/api/predictive1';

  /**
   * @var string
   */
  const GET_ADDRESS_ENDPOINT = '/v2/api/predictive2';

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * ApiClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   The http client factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientFactory $http_client_factory, KeyRepositoryInterface $key_repository, LoggerChannelInterface $logger) {
    $this->configFactory = $config_factory;
    $this->logger = $logger;

    $api_key = $this->configFactory->get('nsw_point_api.settings')->get('api_key');
    if (empty($api_key) ||
      empty($api_key = $key_repository->getKey($api_key)->getKeyValue())) {
      $this->logger->critical('An api key is required in order to use this service.');
    }

    $this->httpClient = $http_client_factory->fromOptions([
      'base_uri' => self::BASE_URI,
      'headers' => ['x-api-key' => $api_key ?? ''],
    ]);
  }

  /**
   * A method to get addresses based on a query.
   *
   * @param array $query_params
   *   The query params to pass as part of the request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getAddresses(array $query_params): ResponseInterface {
    if (empty($query_params['address'])) {
      throw new \Exception("An address query must be provided.");
    }
    $default_query_params = [
      'stateTerritory' => 'All',
      'dataset' => 'all',
    ];
    $query_params = http_build_query(array_merge($default_query_params, $query_params));
    return $this->httpClient->request('GET', self::GET_ADDRESSES_ENDPOINT . '?' . $query_params);
  }

  /**
   * A method to get an address based on an Id.
   *
   * @param array $query_params
   *   The query params to pass as part of the request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getAddress(array $query_params): ResponseInterface {
    if (empty($query_params['id'])) {
      throw new \Exception("An address Id must be provided.");
    }
    $default_query_params = [
      'outFields' => [],
    ];
    $query_params = array_merge($default_query_params, $query_params);
    return $this->httpClient->request('POST', self::GET_ADDRESS_ENDPOINT, ['json' => $query_params]);
  }

}
