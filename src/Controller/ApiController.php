<?php

declare(strict_types = 1);

namespace Drupal\nsw_point_api\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\nsw_point_api\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class ApiController.
 *
 * @package Drupal\nsw_point_api\Controller
 */
class ApiController implements ContainerInjectionInterface {

  /**
   * The logger channel service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $loggerChannel;

  /**
   * The api client service.
   *
   * @var \Drupal\nsw_point_api\ApiClient
   */
  private $apiClient;

  /**
   * ApiController constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   The logger channel service.
   * @param \Drupal\nsw_point_api\ApiClient $api_client
   *   The api client service.
   */
  public function __construct(LoggerChannelInterface $logger_channel, ApiClient $api_client) {
    $this->loggerChannel = $logger_channel;
    $this->apiClient = $api_client;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.channel.nsw_point_api'),
      $container->get('nsw_point_api.api_client'),
    );
  }

  /**
   * A controller action to get addresses using a fuzzy query.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Cache\CacheableResponseInterface
   *   The response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   */
  public function getAddresses(Request $request): CacheableResponseInterface {
    $address = $request->query->get('address');
    $state = $request->query->get('stateTerritory') ?: 'NSW';
    $max_number = $request->query->get('maxNumberOfResults') ?: 5;
    $dataset = $request->query->get('dataset') ?: 'all';
    try {
      $response = $this->apiClient->getAddresses(['address' => $address, 'stateTerritory' => $state, 'maxNumberOfResults' => $max_number, 'dataset' => $dataset]);
      if (Response::HTTP_OK === $response->getStatusCode()) {
        return $this->wrapResponse($response->getBody()->getContents());
      }
    }
    catch (\Exception $e) {
      $this->loggerChannel->warning($e->getMessage());
      throw new BadRequestHttpException("The request could not be handled at this time.");
    }
  }

  /**
   * A controller action to select an address using an Id.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Cache\CacheableResponseInterface
   *   The response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   */
  public function getAddress(Request $request): CacheableResponseInterface {
    $id = $request->query->get('id');
    try {
      $response = $this->apiClient->getAddress(['id' => $id]);
      if (Response::HTTP_OK === $response->getStatusCode()) {
        return $this->wrapResponse($response->getBody()->getContents());
      }
    }
    catch (\Exception $e) {
      $this->loggerChannel->warning($e->getMessage());
      throw new BadRequestHttpException("The request could not be handled at this time.");
    }
  }

  /**
   * A method to wrap the response (json) as cacheable.
   *
   * @param string $response
   *   The raw response.
   *
   * @return \Drupal\Core\Cache\CacheableResponseInterface
   *   The wrapped response.
   */
  private function wrapResponse(string $response): CacheableResponseInterface {
    $cacheable_metadata = (new CacheableMetadata())
      ->addCacheContexts([
        'url.query_args',
      ])
      ->setCacheMaxAge(900);
    $response = CacheableJsonResponse::fromJsonString($response);
    $response->addCacheableDependency($cacheable_metadata);
    return $response;
  }

}
