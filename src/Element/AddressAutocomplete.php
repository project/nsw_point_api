<?php

declare(strict_types = 1);

namespace Drupal\nsw_point_api\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;

/**
 * Class AddressAutocomplete.
 *
 * @package Drupal\nsw_point_api\Element
 *
 * @FormElement("nsw_point_api_address_autocomplete")
 */
class AddressAutocomplete extends Textfield {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return array_merge_recursive(
      parent::getInfo(),
      [
        '#attached' => [
          'library' => ['nsw_point_api/address-autocomplete'],
          'drupalSettings' => [
            'nsw_point_api_address_autocomplete' => [
              'address_not_found' => FALSE,
              'address_not_found_label' => $this->t("Address not found..."),
            ],
          ],
        ],
        '#process' => [
          [$class, 'processAddressAutocomplete'],
        ],
      ]
    );
  }

  /**
   * A process callback method to add drupal settings based on properties.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $complete_form
   *   The complete form.
   *
   * @return array
   *   The element.
   */
  public static function processAddressAutocomplete(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    if (!empty($element['#address_not_found'])) {
      $element['#attached']['drupalSettings']['nsw_point_api_address_autocomplete']['address_not_found'] = $element['#address_not_found'];
    }
    if (!empty($element['#address_not_found_label'])) {
      $element['#attached']['drupalSettings']['nsw_point_api_address_autocomplete']['address_not_found_label'] = $element['#address_not_found_label'];
    }
    $element['#attributes']['class'] = $element['#attributes']['class'] ?? [];
    $element['#attributes']['class'] = array_merge($element['#attributes']['class'], ['nsw-point-api-address-autocomplete']);
    return $element;
  }

}