## Rationale

The module facilitates interaction with the NSW Point API. The purpose of the NSW Point API is to provide a solution for addressing in the context of the government. For more information see https://point.digital.nsw.gov.au/v2/docs/index.html.

### Address Autocomplete
It must be noted that NSW Point API do provide a widget ready to use, however it doesn't use the default jQuery autocomplete widget that ships with Drupal. In addition the 'Address not found' option needs to be customisable and work with a set of fields known to a Drupal form.

#### Use

The element that is provided can be used by specifying its type in the render array:

```
$render_array = [
  '#type' => 'nsw_point_api_address_autocomplete',
];
```

The above will render the element, however the module provides no styling. This is up to the consumer.

Upon typing at least three characters a lookup will be performed and a drop down displayed if there are any results. When a result is selected the element is filled with the value and a custom event triggered in order to allow consumers to act upon this action. The argument given to the custom event is the complete response with the address components and metadata surrounding it. An example of this is available at [https://point.digital.nsw.gov.au/v2/docs/pages/support.html#/default/post_v2_api_predictive2](https://point.digital.nsw.gov.au/v2/docs/pages/support.html#/default/post_v2_api_predictive2).

Alternatively if there are no results you can opt to display an "Address not found" option, which in itself will trigger a separate custom event if selected. By default this is switched off, but can be enabled by supplying the following properties as part of the element:

```
$render_array = [
  '#type' => 'nsw_point_api_address_autocomplete',
  '#address_not_found' => TRUE,
  '#address_not_found_label' => "Address not found...",
];
```

#### Events

The following are custom events triggered in the aforementioned scenarios:

```
nsw-point-api-address-autocomplete:select-address

nsw-point-api-address-autocomplete:select-address-not-found
```

### API Client

The module also provides an API client that can be used to perform actions against the NSW Point API.

To use the API client, simply reference the Drupal service ```nsw_point_api.api_client```.