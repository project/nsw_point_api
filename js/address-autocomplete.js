(function ($, Drupal) {
  Drupal.behaviors.nsw_point_api_address_autocomplete = {
    attach(context, settings) {
      if (context !== document) {
        return;
      }
      const elements = $('.nsw-point-api-address-autocomplete');
      elements.each((index, element) => {
        const autocomplete = $(element).autocomplete({
          minLength: 3,
          source: (request, response) => {
            searchAddress(request.term, response);
          },
          select: (event, ui) => {
            if (ui.item.id) {
              selectAddress.bind(element)(ui.item.id);
            } else {
              selectAddressNotFound.bind(element)();
              return false;
            }
            return true;
          }
        }).data('ui-autocomplete');

        autocomplete._renderItem = (ul, item) => {
          return $("<li>")
            .append($("<div>").html(item.label))
            .addClass(item.class ? item.class : '')
            .data('id', item.id ? item.id : 0)
            .appendTo(ul);
        }
      });

      function searchAddress(searchTerm, response) {
        let searchParameters = `?address=${searchTerm}`;
        let searchUrl = `/nsw_point_api/get-addresses${searchParameters}`;
        let search = new Promise((resolve) => {
          fetch(
            searchUrl,
          ).then((response) => {
            response.json().then((responseJson) => {
              let addresses = [];
              if (responseJson.length > 0) {
                for (const searchResult of responseJson) {
                  addresses.push({
                    label: searchResult.address,
                    value: searchResult.address,
                    class: 'auto-entry',
                    id: searchResult.id,
                  });
                }
              }
              if (settings.nsw_point_api_address_autocomplete.address_not_found && addresses.length === 0) {
                addresses.push({
                  label: settings.nsw_point_api_address_autocomplete.address_not_found_label,
                  value: '',
                  id: false,
                  class: 'no-follow manual-entry',
                });
              }
              resolve(addresses);
            });
          });
        });
        search.then((addresses) => {
          response(addresses);
        });
      }
      function selectAddress(addressId) {
        let selectParameters = `?id=${addressId}`;
        let selectUrl = `/nsw_point_api/get-address${selectParameters}`;
        let select = new Promise((resolve) => {
          fetch(
            selectUrl,
          ).then((response) => {
            response.json().then((responseJson) => {
              resolve(responseJson);
            });
          });
        });
        select.then((response) => {
          const event = new CustomEvent('nsw-point-api-address-autocomplete:select-address', {detail: { response }});
          this.dispatchEvent(event);
        });
      }
      function selectAddressNotFound() {
        const event = new CustomEvent('nsw-point-api-address-autocomplete:select-address-not-found');
        this.dispatchEvent(event);
      }
    }
  };
})(jQuery, Drupal);
